// Fill out your copyright notice in the Description page of Project Settings.


#include "Grippable/GrippableMeshBase.h"

#define TOSERVER(FuncName)\
	if (!bReplicates || GetNetMode() == ENetMode::NM_Standalone) \
	{ \
		Receive##FuncName##(); \
	} \
	else \
	{ \
		if(HasAuthority())	\
		{ \
			##FuncName##_Multicast(false); /* Already server, function wasn't called from client then */ \
		} \
		else \
		{ \
			Receive##FuncName##(); /* Skip replication delay */ \
			##FuncName##_Server(); /* Try multicast use */\
		} \
	}


#define MULTICAST(FuncName) \
	if (bMulticastUseEvents) \
	{ \
		##FuncName##_Multicast(true); /* this function called because of client */ \
	} \
	else \
	{ \
		Receive##FuncName##(); \
	}

// Events



void AGrippableMeshBase::OnUsed_Implementation()
{
	TOSERVER(Use)
}

void AGrippableMeshBase::OnEndUsed_Implementation()
{
	TOSERVER(EndUse);
}

void AGrippableMeshBase::OnSecondaryUsed_Implementation()
{
	TOSERVER(UseSecondary);
}

void AGrippableMeshBase::OnEndSecondaryUsed_Implementation()
{
	TOSERVER(EndUseSecondary);
}

// Server

void AGrippableMeshBase::Use_Server_Implementation()
{
	MULTICAST(Use)
}
void AGrippableMeshBase::EndUse_Server_Implementation()
{
	MULTICAST(EndUse)
}
void AGrippableMeshBase::UseSecondary_Server_Implementation()
{
	MULTICAST(UseSecondary)
}
void AGrippableMeshBase::EndUseSecondary_Server_Implementation()
{
	MULTICAST(EndUseSecondary)
}

// Multicast

void AGrippableMeshBase::Use_Multicast_Implementation(bool bOriginatedFromClient)
{
	if (HasAuthority() || !bOriginatedFromClient || !HasLocalNetOwner())
	{
		ReceiveUse();
	}
}
void AGrippableMeshBase::EndUse_Multicast_Implementation(bool bOriginatedFromClient)
{
	if (HasAuthority() || !bOriginatedFromClient || !HasLocalNetOwner())
	{
		ReceiveEndUse();
	}
}
void AGrippableMeshBase::UseSecondary_Multicast_Implementation(bool bOriginatedFromClient)
{
	if (HasAuthority() || !bOriginatedFromClient || !HasLocalNetOwner())
	{
		ReceiveUseSecondary();
	}
}
void AGrippableMeshBase::EndUseSecondary_Multicast_Implementation(bool bOriginatedFromClient)
{
	if (HasAuthority() || !bOriginatedFromClient || !HasLocalNetOwner())
	{
		ReceiveEndUseSecondary();
	}
}

#undef TOSERVER
#undef MULTICAST
// End Replication 
//////////////////////////////////////////////////////////////////////////

AGrippableMeshBase::AGrippableMeshBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NetUpdateFrequency = 30;
	MinNetUpdateFrequency = 2;
}