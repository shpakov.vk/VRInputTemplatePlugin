// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Grippables/GrippableStaticMeshActor.h"
#include "GrippableMeshBase.generated.h"

/**
 * 
 */
UCLASS()
class COREVRTEMPLATE_API AGrippableMeshBase : public AGrippableStaticMeshActor
{
	GENERATED_BODY()

public: 

	AGrippableMeshBase(const FObjectInitializer& ObjectInitializer); 

	/**
	 * Replicate use events to everyone or run server only
	 * Does not affect Replication from owning Client to Server
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Replication")
	bool bMulticastUseEvents;

public:

	//~ Begin IVRGripInterface
	virtual void OnUsed_Implementation() override final;
	virtual void OnEndUsed_Implementation() override;
	virtual void OnSecondaryUsed_Implementation() override;
	virtual void OnEndSecondaryUsed_Implementation() override;
	//~ End IVRGripInterface

private:

	// Replication
	UFUNCTION(Server, Reliable)
	void Use_Server();
	UFUNCTION(Server, Reliable)
	void EndUse_Server();
	UFUNCTION(Server, Reliable)
	void UseSecondary_Server();
	UFUNCTION(Server, Reliable)
	void EndUseSecondary_Server();

	UFUNCTION(NetMulticast, Reliable)
	void Use_Multicast(bool bOriginatedFromClient);
	UFUNCTION(NetMulticast, Reliable)
	void EndUse_Multicast(bool bOriginatedFromClient);
	UFUNCTION(NetMulticast, Reliable)
	void UseSecondary_Multicast(bool bOriginatedFromClient);
	UFUNCTION(NetMulticast, Reliable)
	void EndUseSecondary_Multicast(bool bOriginatedFromClient);

protected:

	/** Called immediately after use on owning client or from RPC on other clients */
	UFUNCTION(BlueprintImplementableEvent, Category = "Tool", meta = (DisplayName = "ToolUse"))
	void ReceiveUse();

	/** Called immediately after use on owning client or from RPC on other clients */
	UFUNCTION(BlueprintImplementableEvent, Category = "Tool", meta = (DisplayName = "ToolEndUse"))
	void ReceiveEndUse();

	/** Called immediately after use on owning client or from RPC on other clients */
	UFUNCTION(BlueprintImplementableEvent, Category = "Tool", meta = (DisplayName = "ToolUseSecondary"))
	void ReceiveUseSecondary();

	/** Called immediately after use on owning client or from RPC on other clients */
	UFUNCTION(BlueprintImplementableEvent, Category = "Tool", meta = (DisplayName = "ToolEndUseSecondary"))
	void ReceiveEndUseSecondary();
	
};
